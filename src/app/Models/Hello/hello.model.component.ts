import { Component, OnInit, Input } from '@angular/core';
import { Injectable } from '@angular/core';
@Component({
  selector: 'hello-model',
  templateUrl: './hello.model.component.html',
})

@Injectable()
export class HelloModel implements OnInit {
  @Input() helloTitle: string;

  constructor() { 
    //this.helloTitle = title;
  }

  ngOnInit() {
  }

}
