import { Component, OnInit } from '@angular/core';
import { Inject /*permite injeção de dependência*/ } from '@angular/core';

import{ HelloModel } from '../Models/Hello/hello.model.component';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
  arr: Array<HelloModel> = [];

  constructor(model: HelloModel) { 
    
    if(localStorage.hellos != null && localStorage.hellos != null){
      let json = JSON.parse(localStorage.hellos);
      json.forEach(item =>{ this.arr.push(item.HelloModel); });
    }
  }

  ngOnInit() {
  }

}
