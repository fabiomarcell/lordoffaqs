import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './hello.component';
import{ HelloModel } from '../Models/Hello/hello.model.component';
import { HelloRoutingModule } from './hello-routing.module';

@NgModule({
  imports: [
    CommonModule,
    HelloRoutingModule
  ],
  declarations: [HelloComponent, HelloModel],
  providers: [HelloModel]
})
export class HelloModule { }
