import { Component, OnInit } from '@angular/core';
import {HelloModel} from '../Models/Hello/hello.model.component';
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  model: HelloModel = new HelloModel();
  constructor() { }

  ngOnInit() {  }

  onSubmit(){
    if(localStorage.hellos == null || localStorage.hellos == ""){
      localStorage.hellos = "[]";
    }

    let json = JSON.parse(localStorage.hellos);
    json.push(JSON.parse('{"HelloModel":' + JSON.stringify(this.model) + '}'));
    localStorage.hellos = JSON.stringify(json);
    this.model.helloTitle = "";

  }
}
