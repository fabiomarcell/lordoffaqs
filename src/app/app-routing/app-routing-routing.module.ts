import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from '../home/home.component'

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "hello", loadChildren: '../hello/hello.module#HelloModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingRoutingModule { }