import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';

import {NgForm} from '@angular/forms';
import { FormsModule } from '@angular/forms';

import {Router} from '@angular/router';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HelloComponent } from './hello/hello.component';

import{ HelloModel } from './Models/Hello/hello.model.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    NavMenuComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
